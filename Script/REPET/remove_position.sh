#!/bin/bash
# shell to use
#$ -S /bin/bash
#$ -M thomas.chaussepied@wanadoo.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea
# if the script should be run in the current directory
#$ -cwd
## AIMS    : to remove centromere in genome fasta file 
## USAGE   : ./remove_position.sh position.txt genome.fa
## NOTE    : 
## AUTHORS : thomas.chaussepied@wanadoo.fr
#$ -j y


. /local/env/envsamtools-1.6.sh

set -o nounset # exit on unset variable.
set -o errexit # exit on unexpected error.

###############################################################################

declare -r INFILE_pos="${1}" # read input file.
declare -r INFILE_data="${2}" # read input file.

OUTFILE=`echo "${INFILE_data%%.*}"`"_wo_centromere."`echo "${INFILE_data##*.}"`
###############################################################################
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT		= $0\n"
	printf "╟ - INFILE : position	= $INFILE_pos\n"
	printf "╟ - INFILE : data	= $INFILE_data\n"
	printf "╟ - OUTFILE 		= $OUTFILE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

printVars >&2

# =============================================================================


	while read chr beg_cen end_cen end_chr
	do 
		samtools faidx $INFILE_data $chr:1-$beg_cen >> $OUTFILE
		samtools faidx $INFILE_data $chr:$end_cen-$end_chr >> $OUTFILE
	done < $INFILE_pos

###############################################################################

# your work.
printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
