#!/bin/bash

# shell to use
#$ -S /bin/bash

#$ -M thomas.chaussepied@wanadoo.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea
 

# if the script should be run in the current directory
#$ -cwd


## AIMS    : to download forward and reverse fasta files from SRR Id.
## USAGE   : ./SRR_fasta.sh <file/with/SRR/Id.txt> <output/directory>
## NOTE    : /!\ default, only donwload the 10st id. Change "begin" and "end".
## AUTHORS : thomas.chaussepied@wanadoo.fr


# init env X
. /local/env/envsra-tools-2.8.2.sh

###############################################################################

file_id=$1
out_dir=$2

begin=160
end=199


echo Start 
while read SSR; do 
	if [[ "$i" -ge "$begin" ]] && [[ "$i" -le "$end" ]]; then
		fastq-dump --fasta --split-files --outdir $out_dir $SSR
		rm "$HOME"/ncbi/public/sra/"$SSR".sra.cache
	fi
	i=$((i+1))
done < $file_id

###############################################################################
