#!/bin/bash

# shell to use
#$ -S /bin/bash

#$ -M thomas.chaussepied@wanadoo.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea
 

# if the script should be run in the current directory
#$ -cwd



## AIMS    : bgzip all file in a folder .
## USAGE   : ./Bgzip.sh
## NOTE    : 
## AUTHORS : thomas.chaussepied@wanadoo.fr

# init env X
. /local/env/envhtslib-1.6.sh 

###############################################################################

for i in $1/*.fasta; do 
	bgzip "$i" 
done  

###############################################################################
