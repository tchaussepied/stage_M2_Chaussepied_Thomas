# shell to use
#$ -S /bin/bash
#$ -M thomas.chaussepied@wanadoo.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea
# if the script should be run in the current directory
#$ -cwd
## AIMS    : to select a subset of TE annotation
## USAGE   : ./select_TE_from_type.sh annotation.gff3 "TE_type"
## NOTE    : 
## AUTHORS : thomas.chaussepied@wanadoo.fr

set -o nounset # exit on unset variable.
set -o errexit # exit on unexpected error.

###############################################################################


declare -r INFILE="${1}" # read input file.
TE_type=${2}
declare -A TE_array=(
					["DIRS"]="RYX" 
					["LINE"]="RIX" 
					["LTR"]="RLX" 
					["SINE"]="RSX" 
					["Crypton"]="DYX" 
					["Helitron"]="DHX" 
					["Maverick"]="DMX" 
					["TIR"]="DTX" 
					["TRIM"]="TRIM" 
					["LARD"]="LARD" 
					["MITE"]="MITE" 
					["noCat"]="noCat" 
					["?"]="noCat" 
					)

OUTFILE=`echo "${INFILE%%.*}"`"_""$TE_type""."`echo "${INFILE##*.}"`
###############################################################################
function printVars ()
{
        printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
        printf "╟ - SCRIPT				= $0\n"
        printf "╟ - INFILE				= $INFILE\n"
        printf "╟ - OUTFILE				= $OUTFILE\n"
        printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}
# =============================================================================
function main ()
{
	grep "##" $INFILE >> tmp	
	grep $TE_type $INFILE >> tmp
	grep ${TE_array["$TE_type"]} $INFILE >> tmp
	mv tmp $OUTFILE
}
###############################################################################

printVars >&2
# your work.
main
printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0

