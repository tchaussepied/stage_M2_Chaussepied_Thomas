# shell to use
#$ -S /bin/bash
#$ -M thomas.chaussepied@wanadoo.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea
# if the script should be run in the current directory
#$ -cwd
## AIMS    : to add an gff3 ID on fasta file
## USAGE   : ./add_ID_fasta.sh file.fa file.gff3
## NOTE    : 
## AUTHORS : thomas.chaussepied@wanadoo.fr

set -o nounset # exit on unset variable.
set -o errexit # exit on unexpected error.

###############################################################################


declare -r INFILE_fasta="${1}" # read input file.
declare -r INFILE_gff3="${2}" # read input file.
#OUTFILE=`echo "${INFILE%%.*}"`"_""$TE_type""."`echo "${INFILE##*.}"`
###############################################################################
function printVars ()
{
        printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
        printf "╟ - SCRIPT				= $0\n"
        printf "╟ - INFILE : fasta			= $INFILE_fasta\n"
        printf "╟ - INFILE : gff3			= $INFILE_gff3\n"
#        printf "╟ - OUTFILE				= $OUTFILE\n"
        printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

printVars >&2

# =============================================================================
#	grep "##" $INFILE >> tmp	
#	grep $TE_type $INFILE >> tmp
#	grep ${TE_array["$TE_type"]} $INFILE >> tmp
#	mv tmp $OUTFILE
	 
	for id in `grep '>' $INFILE_fasta`
	do
		line_gff3=`grep ${id##*-} $INFILE_gff3`
		ID_line_gff3=`echo $line_gff3 | cut -f9 -d ' '`
		ID_gff3=`echo $ID_line_gff3 | cut -f1 -d ';'`
		ID=`echo ${ID_gff3##*=}`
		new_ID=`echo $id" "$ID`	
		sed -i "s/$id/$new_ID/" $INFILE_fasta
	done 
###############################################################################

# your work.
printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0

