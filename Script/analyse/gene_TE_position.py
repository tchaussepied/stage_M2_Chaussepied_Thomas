from collections import defaultdict as ddict
import argparse


def parse_annotation (path: str, drna: bool) -> dict:
    with open(path, "r") as f_gene:
        # print('opening {0}'.format(path))
        print("Never gonna give you up")
        type_id = 'gene'
        if drna:
            type_id = 'mRNA'
        dict_parse = ddict(lambda: ddict(lambda: ddict(dict)))
        for row in f_gene:
            if row[0] != '#' and ('match' in row or row.split('\t')[2] == type_id) and not ('noCat' in row or 'incomp' in row or 'SSRs' in row or 'Scaffold' in row):
                row = row.split('\t')
                if row[8][3:5] != 'mp':
                    start = int(row[3])
                    stop = int(row[4])
                    id_ele = str(row[8].split(';')[0].split('=')[1])
                    strand = str(row[6])
                    chr_ele = row[0]
                    dict_parse[chr_ele][strand][id_ele] = {'start': start, 'stop': stop}
    return dict_parse


def closed_neighbour (distance_gene_TE: int, distance_closed_TE: int, closed_TE: str, TE: str) -> list:
    """return closest TE to the gene and the distance
    """
    if distance_gene_TE < distance_closed_TE:
        distance_closed_TE = distance_gene_TE
        closed_TE = TE
    nearest_neighbour = [distance_closed_TE, closed_TE]
    return nearest_neighbour


def draw_explanation (path: str) -> None:
    draw = open(path, 'a')
    draw.write("###################################################################################################\n")
    draw.write("#\t0000000000111111111111111222222222222222222223333333333333334444444444\n#\n#")
    draw.write("#\t-------------------------88888888888888888888-------------------------\t\tgene\n#\n")
    draw.write("#\texpected cases\n#\t       -------                                                        \t\t0-1\n")
    draw.write("#\t             -------                                                  \t\t1-1\n")
    draw.write("#\t                      -------                                         \t\t1-2\n")
    draw.write("#\t                                -------                               \t\t2-2\n")
    draw.write("#\t                                          -------                     \t\t2-3\n")
    draw.write("#\t                                                  -------             \t\t3-3\n")
    draw.write("#\t                                                       -------        \t\t3-4\n#\n")

    draw.write("#\textreme cases\n#\t     ---------------------------                                     \t\t0-2\n")
    draw.write("#\t                    ---------------------------                      \t\t1-3\n")
    draw.write("#\t                                      ---------------------------    \t\t2-4\n")
    draw.write("#\t     ---------------------------------------------                   \t\t0-3\n")
    draw.write("#\t                    ---------------------------------------------    \t\t1-4\n")
    draw.write("#\t      --------------------------------------------------------       \t\t0-4\n")
    draw.write("###################################################################################################\n")

    draw.write("0 : upstream out of bound\n1 : upstream\n2 : overlap\n3 : downstream\n4 : downstream out of bound\n\n\n")


def relation_between_elements (sequence_dict: dict, output_path: str, type_1: str = 'genes', type_2: str = 'TEs',
                               distance: int = 5000) -> None:
    out1 = '{0}neighbour_gene_TEs.tsv'.format(output_path)
    out2 = '{0}nearest_neighbour_gene_TEs.tsv'.format(output_path)

    output = open(out1, 'a')
    output_closed = open(out2, 'a')
    draw_explanation(out1)
    draw_explanation(out2)
    output.write("chromosome\tid_gene\tid_TE\tdistance\tposition_start\tposition_stop\n")
    output_closed.write("chromosome\tid_gene\tid_TE\tdistance\tposition\n")
    for chr_ele in sequence_dict[type_1].keys():
        for strand in sequence_dict[type_1][chr_ele].keys():
            print('works chromosome : {0} {1}'.format(chr_ele, strand))
            for gene in sequence_dict[type_1][chr_ele][strand].keys():
                #print('works chromosome : {0} {1} ; gene : {2}'.format(chr_ele, strand, gene))
                start_gene = sequence_dict[type_1][chr_ele][strand][gene]['start']
                start_zone = start_gene - distance
                stop_gene = sequence_dict[type_1][chr_ele][strand][gene]['stop']
                stop_zone = stop_gene + distance

                closed_ante_TE = None
                distance_closed_ante_TE = 999999
                closed_post_TE = None
                distance_closed_post_TE = 999999

                for TE in sequence_dict[type_2][chr_ele][strand].keys():
                    start_TE = sequence_dict[type_2][chr_ele][strand][TE]['start']
                    stop_TE = sequence_dict[type_2][chr_ele][strand][TE]['stop']
                    if start_TE < start_zone:
                        if start_zone < stop_TE < start_gene:
                            distance_gene_TE_ante = start_gene - stop_TE
                            output.write(
                                "{3}\t{0}\t{1}\t{2}\tupstream out of bound\tupstream\n".format(gene, TE,
                                                                                               distance_gene_TE_ante,
                                                                                               chr_ele))
                            tmp_closed_ante_neighbour = closed_neighbour(int(distance_gene_TE_ante),
                                                                         int(distance_closed_ante_TE), closed_ante_TE,
                                                                         TE)
                            closed_ante_TE = tmp_closed_ante_neighbour[1]
                            distance_closed_ante_TE = tmp_closed_ante_neighbour[0]
                        elif start_gene < stop_TE < stop_gene:
                            output.write("{2}\t{0}\t{1}\t0\tupstream out of bound\toverlap\n".format(gene, TE, chr_ele))
                        elif stop_gene < stop_TE < stop_zone:
                            output.write(
                                "{2}\t{0}\t{1}\t0\tupstream out of bound\tdownstream\n".format(gene, TE, chr_ele))
                        elif stop_TE > stop_zone:
                            output.write(
                                "{2}\t{0}\t{1}\t0\tupstream out of bound\tdownstream out of bound\n".format(gene, TE,
                                                                                                            chr_ele))

                    elif start_zone < start_TE < start_gene:
                        if start_zone < stop_TE < start_gene:
                            distance_gene_TE_ante = start_gene - stop_TE
                            output.write(
                                "{3}\t{0}\t{1}\t{2}\tupstream\tupstream\n".format(gene, TE, distance_gene_TE_ante,
                                                                                  chr_ele))
                            tmp_closed_ante_neighbour = closed_neighbour(int(distance_gene_TE_ante), int(distance_closed_ante_TE),
                                                 closed_ante_TE, TE)
                            closed_ante_TE = tmp_closed_ante_neighbour[1]
                            distance_closed_ante_TE = tmp_closed_ante_neighbour[0]
                        elif start_gene < stop_TE < stop_gene:
                            output.write("{2}\t{0}\t{1}\t0\tupstream\toverlap\n".format(gene, TE, chr_ele))
                        elif start_gene < stop_TE < stop_zone:
                            output.write("{2}\t{0}\t{1}\t0\tupstream\tdownstream\n".format(gene, TE, chr_ele))
                        elif stop_TE > stop_zone:
                            output.write(
                                "{2}\t{0}\t{1}\t0\tupstream\tdownstream out of bound\n".format(gene, TE, chr_ele))

                    elif start_gene < start_TE < stop_gene:
                        if start_gene < stop_TE < stop_gene:
                            output.write("{2}\t{0}\t{1}\t0\toverlap\toverlap\n".format(gene, TE, chr_ele))
                        elif stop_gene < stop_TE < stop_zone:
                            output.write("{2}\t{0}\t{1}\t0\toverlap\tdownstream\n".format(gene, TE, chr_ele))
                        elif stop_TE > stop_zone:
                            output.write("{2}\t{0}\t{1}\t0\toverlap'\t'3".format(gene, TE, chr_ele))

                    elif stop_gene < start_TE < stop_zone:
                        if stop_gene < stop_TE < stop_zone:
                            distance_gene_TE_post = start_TE - stop_gene
                            output.write(
                                "{3}\t{0}\t{1}\t{2}\tdownstream\tdownstream\n".format(gene, TE, distance_gene_TE_post,
                                                                                      chr_ele))
                            tmp_closed_post_neighbour = \
                                closed_neighbour(int(distance_gene_TE_post), int(distance_closed_post_TE),
                                                 closed_post_TE, TE)
                            closed_post_TE = tmp_closed_post_neighbour[1]
                            distance_closed_post_TE = tmp_closed_post_neighbour[0]
                        elif stop_TE > stop_zone:
                            distance_gene_TE_post = start_TE - stop_gene
                            output.write(
                                "{3}\t{0}\t{1}\t{2}\tdownstream\tdownstream out of bound\n".format(gene, TE,
                                                                                                   distance_gene_TE_post,
                                                                                                   chr_ele))
                            tmp_closed_post_neighbour = \
                                closed_neighbour(int(distance_gene_TE_post), int(distance_closed_post_TE),
                                                 closed_post_TE, TE)
                            closed_post_TE = tmp_closed_post_neighbour[1]
                            distance_closed_post_TE = tmp_closed_post_neighbour[0]
                output_closed.write(
                    "{3}\t{0}\t{1}\t{2}\tupstream\n".format(gene, closed_ante_TE, distance_closed_ante_TE, chr_ele))
                output_closed.write(
                    "{3}\t{0}\t{1}\t{2}\tdownstream\n".format(gene, closed_post_TE, distance_closed_post_TE, chr_ele))
    output.close()
    output_closed.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("infile_g", type=str, help="file.gff3, with gene position")
    parser.add_argument("-R", "--mRNA", action="store_true", help="genes are determined from mRNA sequences")

    parser.add_argument("infile_TE", type=str, help="file.gff3, with TE position")
    parser.add_argument("output_path", type=str, help="folder path for outputs")
    parser.parse_args()
    args = parser.parse_args()

    print("╔═════════════════════════════════════════════════════════════════════ ═ ═ ═ ═")
    print("╟ - gene FILE= {0}".format(args.infile_g))
    print("╟ - sequence type= {0}".format(args.mRNA))
    print("╟ - TE FILE= {0}".format(args.infile_TE))
    print("╟ - OUTFILE				= {0}neighbour_gene_TEs.tsv".format(args.output_path))
    print("╟ - OUTFILE				= {0}nearest_neighbour_gene_TEs.tsv".format(args.output_path))
    print("╚═════════════════════════════════════════════════════════════════════ ═ ═ ═ ═\n\n")

    sequence = {'genes': parse_annotation(args.infile_g, args.mRNA),
                'TEs': parse_annotation(args.infile_TE, args.mRNA)}

    relation_between_elements(sequence, args.output_path)
#/home/adminigepp/Bureau/bd_brassica/Brapa/Brapa_gene_v1.5.gff
#/home/adminigepp/Bureau/stage_M2_Chaussepied_Thomas/Results/Analyse_data/REPET_Annotation/Brapa/rapa.gff3