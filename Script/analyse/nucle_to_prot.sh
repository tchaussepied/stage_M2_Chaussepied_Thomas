#!/bin/bash
# shell to use
#$ -S /bin/bash

#$ -M thomas.chaussepied@wanadoo.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea
 

# if the script should be run in the current directory
#$ -cwd
#$ -j y

## AIMS    : to translate nuceic fasta file into proteic fasta file and remove wrong ORF i.e with STOP codon
## USAGE   : ./nucle_to_prot nucle.fa 
## NOTE    : /!\ default, only donwload the 10st id. Change "begin" and "end".
## AUTHORS : thomas.chaussepied@wanadoo.fr


# init env X
. /local/env/envexonerate-1.4.0.sh

set -o nounset # exit on unset variable.
set -o errexit # exit on unexpected error.

declare -r INFILE="${1}"
tmp=`echo "${INFILE%%.*}"`".tmp"
OUTFILE=`echo "${INFILE%%.*}"`"_prot_comp_no_chim.fa"

function printVars ()
{
        printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
        printf "╟ - SCRIPT			= $0\n"
        printf "╟ - INFILE 			= $INFILE\n"
        printf "╟ - OUTFILE			= $OUTFILE\n"
        printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

printVars >&2
###############################################################################
# translates nucle to prot
if [[ $INFILE = *".fa" ]]; then 
	fastatranslate $INFILE > $OUTFILE
fi

# puts each sequence on a line
awk '/^>/ {printf("%s%s\t",(N>0?"\n":""),$0);N++;next;} {printf("%s",$0);} END {printf("\n");}' $OUTFILE > $tmp

# remove line with STOP codon '*'
awk -F '\t' '!($2 ~ /\*/)' $tmp > $OUTFILE

# remove incomplete sequence
awk -F '\t' '!($1 ~ /incomp/)' $OUTFILE > $tmp

# remove chimeric sequence
awk -F '\t' '!($1 ~ /chim/)' $tmp > $OUTFILE

# separates header and sequence
sed  's/\t/\n/g' $OUTFILE > $tmp

# cuts sequence every 60 characters
while read line 
	do 
		if [[ $line = '>'* ]]; 
		then 
			echo $line
		else 
			echo $line | sed -re 's/.{60}/&\n/g'  
		fi 
done < $tmp > $OUTFILE
rm $tmp

###############################################################################
printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
