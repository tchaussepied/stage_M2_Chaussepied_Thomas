Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2018-03-08T10:42:12+01:00

====== GenoStack ======


{{./pasted_image.png}} 

chercher REPET (application murano)


{{./pasted_image001.png}}
Création de deux noeuds esclaves (8 core, 16 Gb RAM, 20 Gb HD)
puis suivant

{{./pasted_image002.png}}
Création du noeud maitre ( 1 core , 2 Gb RAM, 20 Gb HD)
l'image c'est Repet-1.2 
mettre le chemin du disque à monter  (trouvable dans Projet > Compute > Share) (chez moi il fait 100 Go)
{{./pasted_image003.png}} mettre l'ip factice 0.0.0.0/0 dans les règles d'accès
{{./pasted_image004.png}}
le chemin est dans le "Path"
puis suivant

{{./pasted_image005.png}}
Création du noued base de donnée ( 1 core , 2 Gb RAM, 20 Gb HD)
puis suivant

{{./pasted_image006.png}}
On peut choisir le nom du projet (ici vu qu'on a déjà monter un disque on s'en moque)
puis suivant

{{./pasted_image007.png}}
Choix du nom de l'application (on s'en moque aussi)
puis suivant

{{./pasted_image008.png}}
Les différents éléments sont paramétrés, cliquer sur "Deploy this Environment "

{{./pasted_image009.png}}
début 08/03/18 10:55
il crée les VMs, les connecte entre elles, injection des clés et initialise le cluster
fin 08/03/18 11:01
{{./pasted_image010.png}}


Aller dans Projet > Compute > Instances 
{{./pasted_image011.png}}
Les 4 VM sont créées.
noter l'ip de Repet-host  (192.168.100.90)

Puis aller sur une console en local pour se [[../Local/Connexion_à_REPET.txt|connecter à REPET]]
